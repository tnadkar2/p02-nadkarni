//
//  ViewController.h
//  p02-2048
//
//  Created by Tejas Nadkarni on 06/02/16.
//  Copyright © 2016 Tejas Nadkarni. All rights reserved.
//

#import <UIKit/UIKit.h>
NSInteger scoreTotal;
Boolean checkAddition = false;

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *blocks;


@property(strong,nonatomic) UIButton *up;
@property (strong, nonatomic) UIButton *down;
@property (strong, nonatomic) IBOutlet UIButton *left;
@property (strong, nonatomic) IBOutlet UIButton *right;
@property (strong, nonatomic) IBOutlet UIButton *reset;
@property (strong, nonatomic) UILabel *score;
@property (strong, nonatomic) IBOutlet UILabel *best;


@end


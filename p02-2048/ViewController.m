//
//  ViewController.m
//  p02-2048
//
//  Created by Tejas Nadkarni on 06/02/16.
//  Copyright © 2016 Tejas Nadkarni. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize blocks;
@synthesize score;
@synthesize best;
NSUserDefaults *bestScore;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Grey-website-background.png"]];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //First random generation
    NSMutableArray *myArray = [NSMutableArray array];
    
    bestScore = [NSUserDefaults standardUserDefaults];
    NSInteger highestScore = [bestScore integerForKey:@"BestScore"];
    [best setText:[NSString stringWithFormat:@"%d", highestScore]];
    
    for(int i = 0; i <= 15; i++) {
        [myArray addObject:[NSNumber numberWithInt:i]];
    }
    
    NSInteger num = [[myArray objectAtIndex:arc4random_uniform(myArray.count)] intValue];
    
    if([[[blocks objectAtIndex:num] text] isEqual:@""])
    {
        [[blocks objectAtIndex:num] setText: @"2"];
        [[blocks objectAtIndex:num] setBackgroundColor: [UIColor colorWithRed:(249/255.0) green:(187/255.0) blue:(104/255.0) alpha:1]];
    }
    [myArray removeAllObjects];
    
    //Second random generation
    //generate random number to continue the game
    [self randomNumberGeneration];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)up:(id)sender {
    checkAddition = false;
    //Stage 1 - get all the location closer
    for (int j=0; j<=3; j++) {
        //Boolean flag = false;
        //this is for location 0,1,2,3
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+4)] text]];
            [[blocks objectAtIndex:(j+4)] setText: @""];
            checkAddition = true;
        }
        
        //this is for location 4,5,6,7
        if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
            checkAddition = true;
        }
        
        //this is for location 8,9,10,11
        if([[[blocks objectAtIndex:j+8] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+8] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
            checkAddition = true;
        }
    }
    
    //Stage 2 - add consecutive numbers in pairs.
    int k = 0;
    while(k<=11)
    {
        
        if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k+4)] text]]) {
            int m = [[[blocks objectAtIndex:k] text] intValue];
            int n = [[[blocks objectAtIndex:(k+4)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                //setting the added value
                [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+4)] setText:@""];
                //calculating score
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        k++;
    }
    
    
    //Stage 3 - move and remove empty space.
    for (int j=0; j<=3; j++) {
        
        //this is for location 0,1,2,3
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+4)] text]];
            [[blocks objectAtIndex:(j+4)] setText: @""];
            checkAddition = true;
        }
        
        //this is for location 4,5,6,7
        if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+8)] text]];
                [[blocks objectAtIndex:(j+8)] setText: @""];
            }
            checkAddition = true;
        }
        
        //this is for location 8,9,10,11
        if([[[blocks objectAtIndex:j+8] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+4] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+4] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+8] setText: [[blocks objectAtIndex:(j+12)] text]];
                [[blocks objectAtIndex:(j+12)] setText: @""];
            }
            checkAddition = true;
        }
    }
    
    //if(checkAddition == true){
        //generate random number to continue the game
        [self randomNumberGeneration];
    //}
}

- (IBAction)down:(id)sender {
    checkAddition = false;
    //Stage 1 - get all the location closer
    for(int dwn = 0;dwn < 4 ; dwn++)
    {
        //last row of columns
        if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
            [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+8)] text]];
            [[blocks objectAtIndex:(dwn+8)] setText:@""];
            checkAddition = true;
        }
        
        //2nd last row of columns
        if([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
                
            }
            else
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
            }
            checkAddition = true;
        }
        //2nd row of column
        if([[[blocks objectAtIndex:(dwn+4)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
               
            }
            else if ([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            else
            {
                [[blocks objectAtIndex:(dwn+4)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            checkAddition = true;
        }
    }
    
    
    //Stage 2 - add consecutive numbers in pairs.
    int k = 15;
    while(k>=4)
    {
        if ([[[blocks objectAtIndex:k] text] isEqualToString: [[blocks objectAtIndex:(k-4)] text]]) {
            int m = [[[blocks objectAtIndex:k] text] intValue];
            int n = [[[blocks objectAtIndex:(k-4)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",result] ];
                
                //emptying the 2nd block
                [[blocks objectAtIndex:(k-4)] setText:@""];
                
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        k--;
    }
    
    
    //Stage 3 - move and remove empty space.
    for(int dwn = 0;dwn < 4 ; dwn++)
    {
        //chking last row of columns
        if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
            [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+8)] text]];
            [[blocks objectAtIndex:(dwn+8)] setText:@""];
           checkAddition = true;
        }
        //checking 2nd last row of columns
        if([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
                
            }
            else
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn+4)] text]];
                [[blocks objectAtIndex:(dwn+4)] setText:@""];
            }
            checkAddition = true;
        }
        //checking 2nd row of column
        if([[[blocks objectAtIndex:(dwn+4)] text] isEqual:@""])
        {
            if ([[[blocks objectAtIndex:(dwn+12)] text] isEqual:@""]) {
                [[blocks objectAtIndex:(dwn+12)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            else if ([[[blocks objectAtIndex:(dwn+8)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(dwn+8)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            else
            {
                [[blocks objectAtIndex:(dwn+4)] setText:[[blocks objectAtIndex:(dwn)] text]];
                [[blocks objectAtIndex:(dwn)] setText:@""];
            }
            checkAddition = true;
        }
    }
    
    //if(checkAddition == true){
        //generate random number to continue the game
        [self randomNumberGeneration];
    //}
}

- (IBAction)left:(id)sender {
    checkAddition = false;
    int j=0;
    //Stage 1 - get all the location closer
    while(j<=12) {
        //for first column of the grid
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+1)] text]];
            [[blocks objectAtIndex:(j+1)] setText: @""];
            checkAddition = true;
        }
        
        //fror the second column of the grid
        if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
            }
            checkAddition = true;
        }
        
        //for the third column of the grid
        if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
            checkAddition = true;
        }
        j=j+4;
    }
    
    //Stage 2 - add consecutive numbers in pairs.
    int k = 0;
    while(k<=12)
    {
        if ([[[blocks objectAtIndex:k] text] isEqualToString:[[blocks objectAtIndex:(k+1)] text]]) {
            int m = [[[blocks objectAtIndex:k] text] intValue];
            int n = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:k] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the 2nd block
                [[blocks objectAtIndex:(k+1)] setText:@""];
                //generating the score
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        if ([[[blocks objectAtIndex:(k+1)] text] isEqualToString:[[blocks objectAtIndex:(k+2)] text]]) {
            int m = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int n = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+2)] setText:@""];
                //generating score
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        if ([[[blocks objectAtIndex:(k+2)] text] isEqualToString:[[blocks objectAtIndex:(k+3)] text]]) {
            int m = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int n = [[[blocks objectAtIndex:(k+3)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the 2nd tile
                [[blocks objectAtIndex:(k+3)] setText:@""];
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        k=k+4;
    }
    
    //Stage 3 - move and remove empty space.
    j=0;
    while(j<=12) {
        
        //this is for location 0
        if([[[blocks objectAtIndex:j] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+1)] text]];
            [[blocks objectAtIndex:(j+1)] setText: @""];
            checkAddition = true;
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
                
            }
            else
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+2)] text]];
                [[blocks objectAtIndex:(j+2)] setText: @""];
            }
            checkAddition = true;
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+3)] text]];
                [[blocks objectAtIndex:(j+3)] setText: @""];
            }
            checkAddition = true;
        }
        j = j+4;
    }
    
    //if(checkAddition == true){
        //generate random number to continue the game
        [self randomNumberGeneration];
    //}
}

//Right click moment of the game
- (IBAction)right:(id)sender {
    checkAddition = false;
    int j=0;
    //Stage 1 - get all the location closer
    while(j<=12) {
        if([[[blocks objectAtIndex:(j+3)] text] isEqual:@""])
        {
            [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j+2)] text]];
            [[blocks objectAtIndex:(j+2)] setText: @""];
            checkAddition = true;
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:(j+2)] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:(j+3)] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:(j+2)] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
            checkAddition = true;
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:(j+1)] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(j+3)] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
            {
                [[blocks objectAtIndex:(j+2)] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:(j+1)] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
            checkAddition = true;
        }
        j=j+4;
    }
    
    //Stage 2 - add consecutive numbers in pairs.
    int k = 0;
    while(k<=12)
    {
        if ([[[blocks objectAtIndex:(k+3)] text] isEqualToString:[[blocks objectAtIndex:(k+2)] text]]) {
            int m = [[[blocks objectAtIndex:k+3] text] intValue];
            int n = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:k+3] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the block
                [[blocks objectAtIndex:(k+2)] setText:@""];
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        if ([[[blocks objectAtIndex:(k+2)] text] isEqualToString:[[blocks objectAtIndex:(k+1)] text]]) {
            int m = [[[blocks objectAtIndex:(k+2)] text] intValue];
            int n = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:(k+2)] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the block
                [[blocks objectAtIndex:(k+1)] setText:@""];
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        if ([[[blocks objectAtIndex:(k+1)] text] isEqualToString:[[blocks objectAtIndex:(k)] text]]) {
            int m = [[[blocks objectAtIndex:(k+1)] text] intValue];
            int n = [[[blocks objectAtIndex:(k)] text] intValue];
            int result = m + n;
            if(result > 0)
            {
                [[blocks objectAtIndex:(k+1)] setText :[NSString stringWithFormat:@"%d",result] ];
                //emptying the block
                [[blocks objectAtIndex:(k)] setText:@""];
                int scoreValue = [score.text intValue];
                scoreTotal = scoreValue + result;
                [score setText:[NSString stringWithFormat:@"%d", scoreTotal]];
            }
        }
        k=k+4;
    }
    
    //Stage 3 - move and remove empty space.
    j=0;
    while(j<=12) {
        
        //this is for location 0
        if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
        {
            [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j+2)] text]];
            [[blocks objectAtIndex:(j+2)] setText: @""];
            checkAddition = true;
        }
        
        //this is for location 4
        if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j+1)] text]];
                [[blocks objectAtIndex:(j+1)] setText: @""];
            }
            checkAddition = true;
        }
        
        //this is for location 8
        if([[[blocks objectAtIndex:j+1] text] isEqual:@""])
        {
            if([[[blocks objectAtIndex:j+3] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+3] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
                
            }
            else if([[[blocks objectAtIndex:j+2] text] isEqual:@""])
            {
                [[blocks objectAtIndex:j+2] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
            else
            {
                [[blocks objectAtIndex:j+1] setText: [[blocks objectAtIndex:(j)] text]];
                [[blocks objectAtIndex:(j)] setText: @""];
            }
            checkAddition = true;
        }
        j = j+4;
    }
    
    //if(checkAddition == true){
        //generate random number to continue the game
        [self randomNumberGeneration];
    //}
}

//Reset Game or start new game
- (IBAction)reset:(id)sender {
    for(int i = 0; i <= 15; i++) {
        [[blocks objectAtIndex:i] setText: @""];
    }
    [self randomNumberGeneration];
    [self randomNumberGeneration];
    [score setText:[NSString stringWithFormat:@"0"]];
}

//random number geneation for getting number 2 at empty positions
- (void) randomNumberGeneration {
    [self changeBlockColor];
    NSMutableArray *myArray = [NSMutableArray array];
    for(int i = 0; i <= 15; i++) {
        if([[[blocks objectAtIndex:i] text] isEqual:@""])
        {
            [myArray addObject:[NSNumber numberWithInt:i]];
        }
    }
    if ([myArray count] != 0)
    {
        NSInteger num = [[myArray objectAtIndex:arc4random_uniform(myArray.count)] intValue];
    
        if([[[blocks objectAtIndex:num] text] isEqual:@""])
        {
            [[blocks objectAtIndex:num] setText: @"2"];
            [[blocks objectAtIndex:num] setBackgroundColor: [UIColor colorWithRed:(249/255.0) green:(187/255.0) blue:(104/255.0) alpha:1]];
        }
        
        [myArray removeAllObjects];
    
    }else{
    
        int count = 0;
        Boolean updateFlag = false;
        int inloop = false;
        //checking for gammeOver
        for (int go=0; go<=11; go++)
        {
            inloop = true;
            count++;
            if([[[blocks objectAtIndex:go] text] isEqual:@""])
            {
                updateFlag = true;
            }
            else
            {
                if((count%4) == 0)
                {
                    if([[[blocks objectAtIndex:go] text] isEqualToString:[[blocks objectAtIndex:(go+4)] text]])
                    {
                        updateFlag = true;
                    }
                }
                else
                {
                    if([[[blocks objectAtIndex:go] text] isEqualToString:[[blocks objectAtIndex:(go+1)] text]])
                    {
                        updateFlag = true;
                    }
                    else if([[[blocks objectAtIndex:go] text] isEqualToString:[[blocks objectAtIndex:(go+4)] text]])
                    {
                        updateFlag = true;
                    }
                }
                
                if(go == 11)
                {
                    if([[[blocks objectAtIndex:12] text] isEqualToString:[[blocks objectAtIndex:13] text]])
                    {
                        updateFlag = true;
                    }
                    else if([[[blocks objectAtIndex:13] text] isEqualToString:[[blocks objectAtIndex:14] text]])
                    {
                        updateFlag = true;
                    }
                    else if([[[blocks objectAtIndex:14] text] isEqualToString:[[blocks objectAtIndex:15] text]])
                    {
                        updateFlag = true;
                    }
                }
            }
        }
        if(!updateFlag && inloop)
        {
            [self showMessage];
        }
    }
}

//apply color change on blocks
-(void) changeBlockColor {
    for(int i = 0; i <= 15; i++) {
        if([[[blocks objectAtIndex:i] text] isEqual:@"2"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(255/255.0) green:(200/255.0) blue:(130/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"4"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(234/255.0) green:(171/255.0) blue:(89/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"8"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(204/255.0) green:(143/255.0) blue:(63/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"16"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(186/255.0) green:(127/255.0) blue:(50/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"32"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(150/255.0) green:(100/255.0) blue:(36/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"64"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(124/255.0) green:(80/255.0) blue:(22/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"128"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(104/255.0) green:(65/255.0) blue:(13/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"256"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(84/255.0) green:(51/255.0) blue:(8/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"512"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(61/255.0) green:(36/255.0) blue:(3/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"1024"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(43/255.0) green:(25/255.0) blue:(1/255.0) alpha:1]];
        }
        else if([[[blocks objectAtIndex:i] text] isEqual:@"2048"])
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(0) green:(0) blue:(0) alpha:1]];
        }
        else
        {
            [[blocks objectAtIndex:i] setBackgroundColor: [UIColor colorWithRed:(173/255.0) green:(172/255.0) blue:(171/255.0) alpha:1]];
        }
    }
}

- (void) showMessage {
    NSString *text = [NSString stringWithFormat: @"YOUR SCORE IS %d", scoreTotal];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"GAME OVER!"
                                                      message: text
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
    [message show];
    [self checkHighScore];
}

-(void) checkHighScore{
    NSInteger theHighScore = [bestScore integerForKey:@"BestScore"];
    if(scoreTotal>theHighScore)
    {
        [bestScore setInteger:scoreTotal forKey:@"BestScore"];
        [best setText:[NSString stringWithFormat:@"%d", scoreTotal]];
       // _highestScore.text= [NSString stringWithFormat:@"%ld", (long)globalScore];
    }
}


@end
